almundo.controller('ListadoCtrl', function AppCtrl($scope, $http) {

  var api_url = "http://localhost:8080";
  var aStarsFilter = [];
  $scope.showNameFilterContent = false;
  $scope.showStarsFilterContent = false;
  $scope.showNameFilterContent = false;
  $scope.hotels = [];

  getHotels = function (){
    $http.get(api_url + '/hotels').success(function (data) {
      $scope.hotels = data;
    });
  };
  getHotels();


  $scope.applyFilter = function (event){

    if (event.target.type == "checkbox") {
      var value = event.target.value;
      if (event.target.checked)
        aStarsFilter.push(value);
      else
        for (var i in aStarsFilter) {
          if (aStarsFilter[i] == value)
            aStarsFilter.splice(i, 1);
        }
    }

    if ($scope.searchField && aStarsFilter.length)
      filter($scope.searchField, aStarsFilter);
     else if ($scope.searchField )
      filter ($scope.searchField, null);
     else if (aStarsFilter)
      filter (null, aStarsFilter);
     else 
      getHotels();
    
  }

  var filter = function (name, stars){
    if (name && stars)
      $http.get(api_url + '/hotels/search?name=' + name + '&stars=' + stars.toString()).success(function (data) {
        $scope.hotels = data;
      });
    else if (name)
      $http.get(api_url + '/hotels/search?name=' + name).success(function (data) {
        $scope.hotels = data;
      });
    else if (stars)
      $http.get(api_url + '/hotels/search?stars=' + stars.toString()).success(function (data) {
        $scope.hotels = data;
      });
    else
      getHotels();
  }

});