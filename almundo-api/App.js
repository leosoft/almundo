// Cargar modulos y crear nueva aplicacion
var express = require("express"); 
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var data = require('./data.json'); // carga de la data del json
 
//Ejemplo: GET http://localhost:8080/hotels
app.get('/hotels', function(req, res) {
  res.send(data);
});
 
//Ejemplo: GET http://localhost:8080/hotels/search?name=ABC&stars=1,2
app.get('/hotels/search', function(req, res) {
  var stars = [];
  var result = [];
  var name = null;

  if (req.query.name && req.query.stars) {
    stars = req.query.stars.split(',');
    
    name = req.query.name;
    var resultNames = data.filter(function(item) {
      return item.name.toLowerCase().includes(name.toLowerCase());
    });
    for (var i in stars){ // Si viene una star en 0 (todas las estrellas,)
      if (stars[i] == 0){
        stars = [];
        result = resultNames;
      }
    }

    for (var i in stars){
      result = result.concat(resultNames.filter(function(item){
        return item.stars == stars[i];
      }));
    }
  } else if (req.query.stars) {
    stars = req.query.stars.split(',');
    for (var i in stars){ // Si viene una star en 0 (todas las estrellas,)
      if (stars[i] == 0){
        stars = [];
        result = data;
      }
    }
    for (var i in stars){
      result = result.concat(data.filter(function(item){
        return item.stars == stars[i];
      }));
    }
  } else if (req.query.name) {
    name = req.query.name;
    result = data.filter(function(item) {
      return item.name.toLowerCase().includes(name.toLowerCase());
    });
  } else {
    return res.send(data);
  }
  
   res.send(result);
});
 
/*//Ejemplo: GET http://localhost:8080/hotels/10
app.get('/hotels/:id', function(req, res) {
  var itemId = req.params.id;
  res.send('Get ' + req.params.id);
});
 
//Ejemplo: POST http://localhost:8080/hotels
app.post('/hotels', function(req, res) {
   var data = req.body.data;
   res.send('Add ' + data);
});
 
//Ejemplo: PUT http://localhost:8080/hotels
app.put('/hotels', function(req, res) {
   var itemId = req.body.id;
   var data = req.body.data;
   res.send('Update ' + itemId + ' with ' + data);
});
 
//Ejemplo: DELETE http://localhost:8080/hotels
app.delete('/hotels/:id', function(req, res) {
   var itemId = req.params.id;
   res.send('Delete ' + itemId);
});*/
  
var server = app.listen(8080, function () {
    console.log('Almundo server is running..'); 
});