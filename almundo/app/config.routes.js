almundo.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/listado', {
                templateUrl: 'partials/listado.view.html',
                controller: 'ListadoCtrl'
            })
            .otherwise({
                redirectTo: '/listado'
            });
    }
]);